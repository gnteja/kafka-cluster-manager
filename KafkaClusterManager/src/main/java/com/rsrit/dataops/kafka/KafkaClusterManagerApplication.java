package com.rsrit.dataops.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaClusterManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaClusterManagerApplication.class, args);
	}
}
