package com.rsrit.dataops.kafka.operations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import com.rsrit.dataops.kafka.dao.KafkaHostDao;
import com.rsrit.dataops.kafka.entity.KafkaHost;

@Component
@EnableAsync
public class InitiateKafkaInstallation {
	
	@Autowired
	KafkaHostDao kafkaHostDao;
	
	@Async
	public void initateKafkaInstallation(KafkaHost templateCredentials, Long projectId) {
		System.out.println("Entered in initiate kafka installation method");
		if(templateCredentials.getHostToolType().equals("kafka")) {
			try {
				//Invoking kafka Installation Scripts 
				invokeKafkaInstallation(templateCredentials, projectId);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void invokeKafkaInstallation(KafkaHost templateCredentials, Long projectId) throws IOException {
		
		System.out.println("Installing Kafka.....!");
		
		ClassPathResource pathForScriptFile = new ClassPathResource("KafkaScripts");
		// Invoke Ansible script for installing kafka
		ProcessBuilder processBuilderForCassandraInstallations = new ProcessBuilder();
		processBuilderForCassandraInstallations
			.directory(pathForScriptFile.getFile());
		
		processBuilderForCassandraInstallations
			.command("sh", "kafkashell.sh",
					templateCredentials.getHostIp(),
					templateCredentials.getHostUsername(),
					templateCredentials.getHostPassword(),
					templateCredentials.getHostPassword());
		Process process = processBuilderForCassandraInstallations.start();

		BufferedReader inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line = null;
		while ((line = inputReader.readLine()) != null) {
			System.out.println(line);
		}
		BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));

		while ((line = errorReader.readLine()) != null) {
			System.out.println(line);
		}
		try {
			process.waitFor();
			if (process.exitValue() == 0) {
				kafkaHostDao.updateStatusOfInstallationByHostIp(templateCredentials.getHostIp(), 1);
				System.out.println("Cassandra installation is successfully completed in " + templateCredentials.getHostIp());
			}else {
				kafkaHostDao.updateStatusOfInstallationByHostIp(templateCredentials.getHostIp(), -1);
				System.out.println("Process exited with code " + process.exitValue());
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
