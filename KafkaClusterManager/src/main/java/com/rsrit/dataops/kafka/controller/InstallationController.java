package com.rsrit.dataops.kafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rsrit.dataops.kafka.dao.KafkaHostDao;
import com.rsrit.dataops.kafka.entity.KafkaHost;
import com.rsrit.dataops.kafka.operations.InitiateKafkaInstallation;


@RestController
@RequestMapping(value= "/installation")
public class InstallationController {
	
	@Autowired
	KafkaHostDao kafkaHostDao;
	
	InitiateKafkaInstallation initiateKafkaInstallation;
	
	public InstallationController(InitiateKafkaInstallation initiateKafkaInstallation) {
		super();
		this.initiateKafkaInstallation = initiateKafkaInstallation;
	}


	@RequestMapping(value="{project_id}/kafka", method= RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void initiateToolInstallation(@RequestBody KafkaHost kafkaHostCredentials,
			@PathVariable("project_id") Long projectId) {
		System.out.println("Entered in installation controller");
		
		//save Kafka Cluster host credentials
		kafkaHostDao.saveKafkaHostCredentials(kafkaHostCredentials, projectId);
		
		//installing Kafka  in the host IP
		initiateKafkaInstallation.initateKafkaInstallation(kafkaHostCredentials, projectId);
	}

}
