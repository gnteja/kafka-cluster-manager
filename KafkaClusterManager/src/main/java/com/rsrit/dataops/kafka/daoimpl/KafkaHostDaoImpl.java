package com.rsrit.dataops.kafka.daoimpl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rsrit.dataops.kafka.dao.KafkaHostDao;
import com.rsrit.dataops.kafka.entity.KafkaHost;
import com.rsrit.dataops.kafka.repo.KafkaHostRepo;

@Service
@Transactional
public class KafkaHostDaoImpl implements KafkaHostDao {
	
	@Autowired
	KafkaHostRepo kafkaHostCredentialsRepo;

	@Override
	public void saveKafkaHostCredentials(KafkaHost kafkaHostCredentials, Long projectId) {
		if(this.kafkaHostCredentialsRepo.findByHostIp(kafkaHostCredentials.getHostIp()) != null) {
			System.out.println("the Host Ip already exists");
		}else {
			if(projectId !=null) {
				kafkaHostCredentials.setProjectId(projectId);
				this.kafkaHostCredentialsRepo.save(kafkaHostCredentials);
			}else {
				System.out.println("projectId cannot be null");
			}
		}
	}

	@Override
	public void updateStatusOfInstallationByHostIp(String hostIp, int status) {
		System.out.println("updating the status oF Kafka installation");
		KafkaHost hostCredentials = this.kafkaHostCredentialsRepo.findByHostIp(hostIp);
		hostCredentials.setStatus(status);
		this.kafkaHostCredentialsRepo.save(hostCredentials);	
	}

	@Override
	public KafkaHost findByHostIp(String hostIp) {
		System.out.println("Sql Server credentials using Host IP");
		return this.kafkaHostCredentialsRepo.findByHostIp(hostIp);
	}

}
