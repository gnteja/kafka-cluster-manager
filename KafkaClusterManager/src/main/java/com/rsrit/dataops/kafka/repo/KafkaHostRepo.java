package com.rsrit.dataops.kafka.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rsrit.dataops.kafka.entity.KafkaHost;

@Repository
public interface KafkaHostRepo extends JpaRepository<KafkaHost, Long> {
	KafkaHost findByHostIp(String hostIp);
}
