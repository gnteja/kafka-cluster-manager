package com.rsrit.dataops.kafka.dao;

import com.rsrit.dataops.kafka.entity.KafkaHost;

public interface KafkaHostDao {
	
	public void saveKafkaHostCredentials(KafkaHost kafkaHostCredentials, Long projectId);
	public void updateStatusOfInstallationByHostIp(String hostIp, int status);
	public KafkaHost findByHostIp(String hostIp);

}
