#!/bin/bash

targetIp=$1
target_username=$2
target_password=$3
target_sudopassword=$4

ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook playbook.yaml -i $targetIp, -e "ansible_ssh_user=$target_username ansible_ssh_pass=$target_password ansible_sudo_pass=$target_sudopassword"